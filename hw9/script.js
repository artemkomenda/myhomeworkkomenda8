
// Опишіть, як можна створити новий HTML тег на сторінці. --- document.createElement("")
// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра. --- Розбирає вказівний текст HTML і вводить отримані узли  в дерево DOM в указаній позиці
// Як можна видалити елемент зі сторінки?  --- document.remove()

let examples = [ "hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv",];
let examples1 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const createLiForItem = (el) => {
    let li = document.createElement('li')
    li.innerHTML = el
    return li
}
const createUlForItems = (el) => {
    let ul = document.createElement('ul')
    ul.append(...el)
    return ul
}

function applyList(array, node) {
    window.onload = () => {
        let list = array.map((el) => {
            return Array.isArray(el) ? createUlForItems(el.map(createLiForItem)) : createLiForItem(el)
        })

        const parentNode = node || document.body
        parentNode.append(...list)
    }
}

applyList(examples)
applyList(examples1)

