
// Опишіть своїми словами що таке Document Object Model (DOM)  --- допомагає нам зробити різні оформлення , зміну інтерфейсу, та багато всього іншогою
// Яка різниця між властивостями HTML-елементів innerHTML та innerText? --- innerHTML --тільки для дочірніх елементів а innerText для parent
// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?  ----  document.querySelector




let allParagraf = document.querySelectorAll("p")
console.log(allParagraf)
// allParagraf.style.backgroundColor= '#ff0000';

let optionsList = document.querySelector("#optionsList")
console.log(optionsList)

let child = optionsList.childNodes[0] === optionsList.firstChild;
console.log(child)


for(let node of document.body.childNodes){
    console.log(node)
}

let testParagraph = document.getElementsByClassName("testParagraph")
console.log(testParagraph)

let newTextTestParagraph = testParagraph.innerHTML = "<p>This is a paragraph</p>"
console.log(newTextTestParagraph)

let mainHeader = document.querySelector(".main-header")
mainHeader.classList.add("nav-item")
console.log(mainHeader)

let sectionTitle = document.querySelectorAll(".section-options")
console.log(sectionTitle)

let newSectionTitle = sectionTitle.classList.remove("section-title")
console.log(newSectionTitle)